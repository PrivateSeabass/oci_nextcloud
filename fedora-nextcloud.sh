#!/bin/sh

podman network create nextcloud-net
#podman pod create \
#  --publish 8080:80 \
#  --name=nextpod

podman run --detach \
  --env MYSQL_DATABASE=nextcloud \
  --env MYSQL_USER=nextcloud \
  --env MYSQL_PASSWORD=test \
  --env MYSQL_ROOT_PASSWORD=test_root \
  --volume nextcloud-db:/var/lib/mysql \
  --network=nextcloud-net \
  --restart on-failure \
  --name nextcloud-db \
  docker.io/library/mariadb:10

podman run --detach \
  --env MYSQL_HOST=nextcloud-db.dns.podman \
  --env MYSQL_DATABASE=nextcloud \
  --env MYSQL_USER=nextcloud \
  --env MYSQL_PASSWORD=test \
  --env NEXTCLOUD_ADMIN_USER=ncadmin \
  --env NEXTCLOUD_ADMIN_PASSWORD=testncadmin \
  --env NEXTCLOUD_TRUSTED_DOMAINS=manager.cloudart.moe \
  --volume nextcloud-app:/var/www/html \
  --volume nextcloud-data:/var/www/html/data \
  --network=nextcloud-net \
  --restart on-failure \
  --name nextcloud \
  --publish 8080:80 \
  docker.io/library/nextcloud:20
